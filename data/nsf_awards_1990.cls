69 Life Sciences NEC
71 Psychology Biological Aspects
72 Psychology Social Aspects
10 Physical Sciences
11 Astronomy
12 Chemistry
13 Physics
79 Psychology Sciences NEC
80 Social Sciences
81 Anthropology
82 Economics
19 Physical Sciences NEC
83 History
84 Linguistics
21 Mathematics
85 Political Sciences
86 Sociology
87 Law
88 Geography
89 Social Sciences NEC
91 Science Technology Assess
92 Science Policy
30 Computer Science & Engineering
31 Computer Science & Engineering
99 Other Sciences NEC
40 Environmental Sciences
41 Atmospheric Sciences
42 Geological Sciences
43 Biological Oceanography
44 Physical & Chemical Oceanography
45 Ecology
49 Environmental NEC
50 Engineering
51 Engineering-Aeronautical
53 Engineering-Chemical
54 Engineering-Civil
55 Engineering-Electrical
56 Engineering-Mechanical
57 Engineering-Metallurgy & Material
59 Engineering NEC
61 Life Science Biological
63 Life Science Other Medical
