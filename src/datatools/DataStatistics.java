package datatools;

public class DataStatistics {
	
	public int NSequences;
	public int NLabels;
	public double AvgSeqLength;
	public int MinSeqLength;
	public int MaxSeqLength;
	public double AvgLabFreq;
	public int MinLabFreq;
	public int MaxLabFreq;
	public double StdDevFreq;
	public int FreqUpperThreshold;
	public int FreqLowerThreshold;
}
